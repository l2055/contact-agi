import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ContactServiceTest {
    private ContactService contactService = new ContactService();

    @Test
    void shouldFailTooLong() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> contactService.creerContact("abcdefghiklmabcdefghiklmabcdefghiklmabcdefghiklmabcdefghiklm1"));
    }

    @Test
    void shouldFailTooShort() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> contactService.creerContact("ab"));
    }

    @Test
    void shouldFailEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> contactService.creerContact(""));
    }
}